extern crate clap;
extern crate pwhash;
extern crate rpassword;
extern crate users;
extern crate capabilities;

use clap::*;
use std::fs::File;
use std::io::*;
use pwhash::sha512_crypt;
use std::fs::OpenOptions;
use capabilities::{Capabilities};


fn main() {
    let current_user = &users::get_current_username().unwrap();

    let matches = App::new("passwd-rs")
        .version(".01")
        .author("Taha H. <tahahawa@gmail.com>")
        .about("implements some passwd stuff")
        .arg(
            Arg::with_name("SHADOW")
                .help("Sets the input file to use")
                .required(true)
                .index(1)
                .default_value("/etc/shadow"),
        )
        .arg(
            Arg::with_name("user")
                .short("u")
                .long("user")
                .help("Sets the user file to change password for")
                .required(false)
                .takes_value(true)
                .default_value(current_user),
        )
        .arg(
            Arg::with_name("Capabilities")
            .short("c")
            .help("Lists capabilities of the current process")
            .required(false)
        )
        .get_matches();

    if matches.is_present("Capabilities") {
        println!("{}", Capabilities::from_current_proc().unwrap());
    }

    let shadow = File::open(matches.value_of("SHADOW").unwrap()).unwrap();
    let mut hash_str = "".to_owned();
    let mut shadow_line = "".to_owned();

    // let mut reader = BufReader::new(shadow.try_clone().unwrap());
    let mut reader = BufReader::new(shadow);

    let mut lines: Vec<String> = reader.by_ref().lines().map(|x| x.unwrap()).collect();

    for line in lines.clone() {
        let splits = line.split(':').collect::<Vec<&str>>();

        if splits[0] == matches.value_of("user").unwrap() {
            shadow_line = line.to_owned();
            hash_str = splits[1].to_owned();
            break;
        }
    }

    drop(reader);

    let mut current_password = "".to_owned();
    if current_user == "root" {} 
    else {
        current_password = rpassword::prompt_password_stdout("Current Password: ").unwrap();
    }
    

    if sha512_crypt::verify(&current_password, &hash_str) || current_user == "root" {
        println!("Password matches");

        let new_password = rpassword::prompt_password_stdout("New Password: ").unwrap();
        let confirm_password = rpassword::prompt_password_stdout("Confirm Password: ").unwrap();
        if new_password == confirm_password {
            let new_hash = sha512_crypt::hash_with(&hash_str as &str, &new_password).unwrap();

            let mut shadow_line_vec = shadow_line.split(":").collect::<Vec<&str>>();

            let _ = shadow_line_vec[1] = &new_hash;
            // println!("{}", new_hash);
            // println!("{:?}", shadow_line_vec.join(":"));

            let mut shadow = OpenOptions::new().create(true).append(false).write(true).open(matches.value_of("SHADOW").unwrap()).unwrap();

            lines.retain(|e| !(e == &shadow_line) );
            let _ = lines.push(shadow_line_vec.join(":"));

            let _ = shadow.seek(SeekFrom::Start(0));

            let _ = shadow.write_all(lines.join("\n").as_bytes());

        } else {
            println!("Confirmation Password does not match");
            return ();
        }
    } else {
        println!("Password does not match");
        return ();
    }
}
